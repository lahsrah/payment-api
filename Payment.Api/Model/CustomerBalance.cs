﻿namespace Payment.Api.Model
{
    public class CustomerBalance
    {
        public decimal Amount { get; set; }
    }
}
