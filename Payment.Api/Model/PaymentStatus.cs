﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Api.Model
{
    public enum PaymentStatus
    {
        Pending = 1,
        Closed = 2,
        Processed = 3
    }
}
