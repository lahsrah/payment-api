﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Payment.Api.Model
{
    public class PaymentRequest
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public PaymentStatus Status { get; private set; } = PaymentStatus.Pending;
        public string ClosedReason { get; set; }


        public static class ClosedReasons
        {
            public const string NotEnoughFounds = "Not enough funds";
        }

        public void Process()
        {
            if(Status != PaymentStatus.Pending)
            {
                throw new ValidationException("Can only process a payment that is pending"); 
            }

            Status = PaymentStatus.Processed;
        }

        public void Close(string closedReason)
        {
            if(Status == PaymentStatus.Processed)
            {
                throw new ValidationException("Cannot close a payment that has already been processed");
            }

            Status = PaymentStatus.Closed;
            ClosedReason = closedReason;
        }

        public void Cancel(string reason)
        {
            if(Status == PaymentStatus.Pending)
            {
                Close(reason);
            }
            else
            {
                throw new ValidationException("Cannot cancel a payment that isn't pending");
            }
        }
    }
}
