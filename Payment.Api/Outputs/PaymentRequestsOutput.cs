﻿using Payment.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Api
{
    public class PaymentRequestsOutput
    {
        public List<PaymentRequestDto> PaymentRequests { get; set; }
        public decimal AccountBalance { get; set; }
    }

    public class PaymentRequestDto
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public PaymentStatus Status { get; set; }
        public string Reason { get; set; }
    }
}
