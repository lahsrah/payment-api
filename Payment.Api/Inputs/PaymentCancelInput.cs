﻿namespace Payment.Api.Inputs
{
    public class PaymentCancelInput
    {
        public string Reason { get; set; }
    }
}
