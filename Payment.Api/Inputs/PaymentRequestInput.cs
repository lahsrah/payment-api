﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Payment.Api.Inputs
{
    public class PaymentRequestInput
    {
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }

        public void Validate()
        {
            if(Amount <= 0)
            {
                throw new ValidationException("Amount must be positive");
            }

            if (Date == DateTime.MinValue)
            {
                throw new ValidationException("Payment Date must be supplied");
            }
        }
    }
}
