﻿using Payment.Api.Data;
using Payment.Api.Inputs;
using Payment.Api.Model;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Payment.Api.Services
{
    public interface IPaymentService
    {
        PaymentRequest SubmitPaymentRequest(PaymentRequestInput request);
        PaymentRequestsOutput GetAllRequests();

        void Cancel(Guid paymentRequestId, string reason);
        void Process(Guid paymentRequestId);
    }

    public class PaymentService : IPaymentService
    {
        private IRepository _repository;

        public PaymentService(IRepository repository)
        {
            _repository = repository;
        }

        public PaymentRequestsOutput GetAllRequests()
        {
            var requests = _repository.AllRequests().Select(x => new PaymentRequestDto
            {
                Id = x.Id,
                Amount = x.Amount,
                Date = x.Date,
                Status = x.Status,
                Reason = x.ClosedReason
            });

            return new PaymentRequestsOutput
            {
                PaymentRequests = requests.OrderByDescending(p => p.Date).ToList(),
                AccountBalance = _repository.AccountBalance
            };
        }

        public PaymentRequest SubmitPaymentRequest(PaymentRequestInput request)
        {
            var paymentRequest = new PaymentRequest { Amount = request.Amount, Date = request.Date };

            var accountBalance = _repository.AccountBalance;

            if(!HasSufficientBalance(accountBalance, request.Amount))
            {
                paymentRequest.Close(PaymentRequest.ClosedReasons.NotEnoughFounds);
            }

            _repository.Add(paymentRequest);

            return paymentRequest;
        }

        public void Process(Guid paymentRequestId)
        {
            var request = _repository.FindById(paymentRequestId);

            if (request == null) throw new ValidationException("Payment not found");

            request.Process();
            _repository.AccountBalance -= request.Amount;
        }

        public void Cancel(Guid paymentRequestId, string reason)
        {
            var request = _repository.FindById(paymentRequestId);

            if (request == null) throw new ValidationException("Payment not found");

            request.Cancel(reason);
        }

        private bool HasSufficientBalance(decimal accountBalance, decimal amount)
        {
            return accountBalance >= amount;
        }
    }
}
