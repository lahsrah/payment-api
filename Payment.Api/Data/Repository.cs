﻿using Payment.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.Api.Data
{
    public interface IRepository
    {
        public decimal AccountBalance { get; set; }

        PaymentRequest FindById(Guid paymentRequestId);
        void Add(PaymentRequest request);
        void SetBalance(decimal startingBalance);
        List<PaymentRequest> AllRequests();
    }

    public class Repository : IRepository
    {
        private List<PaymentRequest> PaymentRequests { get; set; } = new List<PaymentRequest>();
        public decimal AccountBalance { get; set; }

        public void SetBalance(decimal startingBalance)
        {
            AccountBalance = startingBalance;
        }

        public PaymentRequest FindById(Guid paymentRequestId)
        {
            return PaymentRequests.FirstOrDefault(p => p.Id == paymentRequestId);
        }

        public List<PaymentRequest> AllRequests() => PaymentRequests;

        public void Add(PaymentRequest request)
        {
            PaymentRequests.Add(request);
        }
    }
}
