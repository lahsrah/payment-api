﻿using Microsoft.AspNetCore.Mvc;
using Payment.Api.Inputs;
using Payment.Api.Services;
using System;

namespace Payment.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentRequestController : ControllerBase
    {
        private readonly IPaymentService _paymentService;

        public PaymentRequestController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        [HttpGet]
        public PaymentRequestsOutput Get()
        {
            return _paymentService.GetAllRequests();
        }

        [HttpPost]
        public void Post(PaymentRequestInput input)
        {
            input.Validate();

            _paymentService.SubmitPaymentRequest(input);
        }

        [HttpPost]
        [Route("{id}/cancel")]
        public void Post(Guid id, PaymentCancelInput input)
        {
            _paymentService.Cancel(id, input.Reason);
        }

        [HttpPost]
        [Route("{id}/process")]
        public void Post(Guid id)
        {
            _paymentService.Process(id);
        }
    }
}
