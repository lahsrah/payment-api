using Payment.Api.Data;
using Payment.Api.Inputs;
using Payment.Api.Model;
using Payment.Api.Services;
using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace Payment.Api.Tests
{
    public class PaymentRequest_Process_Tests
    {
        private IRepository _repository;

        public PaymentRequest_Process_Tests()
        {
            _repository = new Repository();
            _repository.SetBalance(100);
        }

        [Fact]
        public void PaymentRequest_ProcessPendingRequest_IsProcessed()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 100 });

            Assert.Equal(PaymentStatus.Pending, payment.Status);

            paymentService.Process(payment.Id);

            Assert.Equal(PaymentStatus.Processed, payment.Status);
        }

        [Fact]
        public void PaymentRequest_ProcessClosedRequest_ThrowsException()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 101 });

            Assert.Equal(PaymentStatus.Closed, payment.Status);
            Assert.Throws<ValidationException>(() => paymentService.Process(payment.Id));
        }
    }
}
