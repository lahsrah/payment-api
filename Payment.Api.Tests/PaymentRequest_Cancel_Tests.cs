using Payment.Api.Data;
using Payment.Api.Inputs;
using Payment.Api.Model;
using Payment.Api.Services;
using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace Payment.Api.Tests
{
    public class PaymentRequest_Cancel_Tests
    {
        private IRepository _repository;

        public PaymentRequest_Cancel_Tests()
        {
            _repository = new Repository();
            _repository.SetBalance(100);
        }

        [Fact]
        public void PaymentRequest_CancelPendingRequest_IsClosedWithReason()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 100 });

            Assert.Equal(PaymentStatus.Pending, payment.Status);

            paymentService.Cancel(payment.Id, "Reason");

            Assert.Equal(PaymentStatus.Closed, payment.Status);
            Assert.Equal("Reason", payment.ClosedReason);
        }

        [Fact]
        public void PaymentRequest_CancelClosedRequest_ThrowsException()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 200 });
            Assert.Equal(PaymentStatus.Closed, payment.Status);

            Assert.Throws<ValidationException>(() => paymentService.Cancel(payment.Id, "Reason"));
        }

        [Fact]
        public void PaymentRequest_CancelProcessedRequest_ThrowsException()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 100 });
            paymentService.Process(payment.Id);
            Assert.Equal(PaymentStatus.Processed, payment.Status);

            Assert.Throws<ValidationException>(() => paymentService.Cancel(payment.Id, "Reason"));
        }
    }
}
