using Payment.Api.Data;
using Payment.Api.Inputs;
using Payment.Api.Model;
using Payment.Api.Services;
using System;
using Xunit;

namespace Payment.Api.Tests
{
    public class PaymentRepositoryTests
    {
        private IRepository _repository;

        public PaymentRepositoryTests()
        {
            _repository = new Repository();
            _repository.SetBalance(100);
        }

        [Fact]
        public void PaymentRepository_Empty_When_Application_Initialized_InitialBalanceCorrect()
        {
            Assert.Empty(_repository.AllRequests());
            Assert.Equal(100, _repository.AccountBalance);
        }
        
        [Fact]
        public void PaymentRepository_Can_Add_Payment()
        {
            var paymentService = new PaymentService(_repository);

            Assert.Empty(_repository.AllRequests());

            paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 50 });
            Assert.Single(_repository.AllRequests());
        }

        [Fact]
        public void PaymentRepository_Can_Add_Multiple_Payments()
        {
            var paymentService = new PaymentService(_repository);
            paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 50 });
            paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 100 });
            paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 200 });

            Assert.Equal(3, _repository.AllRequests().Count);
        }
    }
}
