using Payment.Api.Data;
using Payment.Api.Inputs;
using Payment.Api.Model;
using Payment.Api.Services;
using System;
using Xunit;

namespace Payment.Api.Tests
{
    public class PaymentRequest_GetAll_Tests
    {
        private IRepository _repository;

        public PaymentRequest_GetAll_Tests()
        {
            _repository = new Repository();
            _repository.SetBalance(500);
            var paymentService = new PaymentService(_repository);
            paymentService.SubmitPaymentRequest(new PaymentRequestInput
            {
                Amount = 10,
                Date = new DateTime(2020, 7, 1)
            });
            paymentService.SubmitPaymentRequest(new PaymentRequestInput
            {
                Amount = 20,
                Date = new DateTime(2020, 7, 2)
            });
            paymentService.SubmitPaymentRequest(new PaymentRequestInput
            {
                Amount = 30,
                Date = new DateTime(2020, 7, 3)
            });
        }

        [Fact]
        public void PaymentRequest_GetAll_ReturnsCorrectPaymentsSortedByMostRecentFirstAndBalance()
        {
            var paymentService = new PaymentService(_repository);
            var result = paymentService.GetAllRequests();

            Assert.Equal(3, result.PaymentRequests.Count);

            
            Assert.Equal(30, result.PaymentRequests[0].Amount);
            Assert.Equal(20, result.PaymentRequests[1].Amount);
            Assert.Equal(10, result.PaymentRequests[2].Amount);


            Assert.Equal(PaymentStatus.Pending, result.PaymentRequests[0].Status);
            Assert.Equal(PaymentStatus.Pending, result.PaymentRequests[1].Status);
            Assert.Equal(PaymentStatus.Pending, result.PaymentRequests[2].Status);


            Assert.Equal(new DateTime(2020, 7, 3), result.PaymentRequests[0].Date);
            Assert.Equal(new DateTime(2020, 7, 2), result.PaymentRequests[1].Date);
            Assert.Equal(new DateTime(2020, 7, 1), result.PaymentRequests[2].Date);

            Assert.Equal(500, result.AccountBalance);
        }


        [Fact]
        public void PaymentRequest_GetAll_AfterProcessingRequests_ReturnsCorrectPaymentsSortedByMostRecentFirstAndBalance()
        {
            var paymentService = new PaymentService(_repository);

            foreach(var request in _repository.AllRequests())
            {
                paymentService.Process(request.Id);
            }

            var result = paymentService.GetAllRequests();

            Assert.Equal(3, result.PaymentRequests.Count);

            Assert.Equal(30, result.PaymentRequests[0].Amount);
            Assert.Equal(20, result.PaymentRequests[1].Amount);
            Assert.Equal(10, result.PaymentRequests[2].Amount);


            Assert.Equal(PaymentStatus.Processed, result.PaymentRequests[0].Status);
            Assert.Equal(PaymentStatus.Processed, result.PaymentRequests[1].Status);
            Assert.Equal(PaymentStatus.Processed, result.PaymentRequests[2].Status);


            Assert.Equal(new DateTime(2020, 7, 3), result.PaymentRequests[0].Date);
            Assert.Equal(new DateTime(2020, 7, 2), result.PaymentRequests[1].Date);
            Assert.Equal(new DateTime(2020, 7, 1), result.PaymentRequests[2].Date);

            Assert.Equal(440, result.AccountBalance);

        }


    }
}
