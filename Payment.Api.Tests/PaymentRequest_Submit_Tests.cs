using Payment.Api.Data;
using Payment.Api.Inputs;
using Payment.Api.Model;
using Payment.Api.Services;
using Xunit;

namespace Payment.Api.Tests
{
    public class PaymentRequest_Submit_Tests
    {
        private IRepository _repository;

        public PaymentRequest_Submit_Tests()
        {
            _repository = new Repository();
            _repository.SetBalance(100);
        }

        [Fact]
        public void PaymentRequest_Submit_PaymentEqualToBalance_IsPending()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 100 });

            Assert.Equal(PaymentStatus.Pending, payment.Status);
        }

        [Fact]
        public void PaymentRequest_Submit_PaymentLessThanBalance_IsPending()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 1 });

            Assert.Equal(PaymentStatus.Pending, payment.Status);
        }

        [Fact]
        public void PaymentRequest_Submit_PaymentAboveBalance_IsClosedImmediately()
        {
            var paymentService = new PaymentService(_repository);
            var payment = paymentService.SubmitPaymentRequest(new PaymentRequestInput { Amount = 101 });
            
            Assert.Equal(PaymentStatus.Closed, payment.Status);
            Assert.Equal(PaymentRequest.ClosedReasons.NotEnoughFounds, payment.ClosedReason);
        }
    }
}
