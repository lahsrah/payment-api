﻿How to build/run the API

- Install .NET Core SDK 3.1
- Navigate to the root directory of the repository with powershell and run `dotnet run --project ./Payment.Api/Payment.Api.csproj`

How to run Unit and Integration Tests

- In Powershell from the root directory of the repository with run `dotnet test`

Assumptions

- Balance check is only required during submitting a payment, all pending payments are approved regardless of account balance when processing them.
- Payment amounts must be positive
- Payment date can be any date including past dates
- Application upon launching initialises the customer's account balance to 500, this can be found in `Startup.cs`.

Architecture

- Web API build with .NET Core 3
- XUnit Framework used for tests
- Data store used is an in memory collection and is injected as a singleton, all data is lost when the application restarts
- There is currently no authentication and I am assuming there is a single user to the system for the sake of simplicity (only one balance and set of payments are stored), when multiple user support is added the data store will also need to change
- All payments actions are performed via a PaymentService and Unit tests are written for all operations on the Payment
- Integration tests are written for all public facing operations of the API but could use more test scenarios and refactoring to streamline setting up the data/state needed for a test, it is currently done by performing actions on the API within the same test but could be broken out to improve test readibility.

CI/CD

- This API can be built using Azure DevOps as a standard asp.net core application and deployed to an Azure App Service.
- There are currently no external dependencies to the API so to deploy to multiple environment it would be a matter of deploying to separate Azure App Services
- There will be further work required to implement a persistant data store instead of the current in memory store where data is lost when the app restarts
- Azure ARM Templates can be added to automate creation of Azure App Service resources
