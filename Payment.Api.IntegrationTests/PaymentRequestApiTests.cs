using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Payment.Api.IntegrationTests
{
    public class PaymentRequestApiTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;

        public PaymentRequestApiTests()
        {
            _server = new TestServer(new WebHostBuilder().UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task GetPayments_ReturnsEmptyList_InitialBalance()
        {
            var response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            var output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);

            Assert.Equal(500, output.AccountBalance);
            Assert.Empty(output.PaymentRequests);
        }

        [Fact]
        public async Task AddPaymentRequest_DateMissing_ReturnsBadRequest()
        {
            var payload = new { Amount = 100 }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, postResponse.StatusCode);

            var error = await postResponse.Content.ReadAsStringAsync();
            Assert.Equal("Payment Date must be supplied", error);
        }

        [Fact]
        public async Task AddPaymentRequest_AmountZero_ReturnsBadRequest()
        {
            var payload = new { Amount = 0, Date = DateTime.Now }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, postResponse.StatusCode);

            var error = await postResponse.Content.ReadAsStringAsync();
            Assert.Equal("Amount must be positive", error);
        }

        [Fact]
        public async Task AddPaymentRequest_AmountNegative_ReturnsBadRequest()
        {
            var payload = new { Amount = -100, Date = DateTime.Now }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, postResponse.StatusCode);

            var error = await postResponse.Content.ReadAsStringAsync();
            Assert.Equal("Amount must be positive", error);
        }

        [Fact]
        public async Task AddPaymentRequest_EmptyRequest_ReturnsBadRequest()
        {
            var payload = new { }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, postResponse.StatusCode);
        }

        [Fact]
        public async Task AddPaymentRequest_IsSuccessful()
        {
            var payload = new { Amount = 100, Date = new DateTime(2020, 8, 20) }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            postResponse.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task CancelPaymentRequest_InvalidPaymentId_ReturnsBadRequest()
        {
            var payload = new { Reason = "Cancelling this" }.AsJson();

            var postResponse = await _client.PostAsync($"/paymentrequest/{Guid.Empty}/cancel", payload);

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, postResponse.StatusCode);
        }

        [Fact]
        public async Task ProcessPaymentRequest_InvalidPaymentId_ReturnsBadRequest()
        {
            var payload = new { }.AsJson();

            var postResponse = await _client.PostAsync($"/paymentrequest/{Guid.Empty}/process", payload);

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, postResponse.StatusCode);
        }


        [Fact]
        public async Task AddPaymentRequest_IsSuccessful_PaymentIsPending()
        {
            var payload = new { Amount = 100, Date = new DateTime(2020, 8, 20) }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            postResponse.EnsureSuccessStatusCode();


            var response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);

            Assert.Equal(500, output.AccountBalance);
            Assert.Single(output.PaymentRequests);

            Assert.Equal(100, output.PaymentRequests[0].Amount);
            Assert.Equal(new DateTime(2020, 8, 20), output.PaymentRequests[0].Date);
            Assert.Equal(Model.PaymentStatus.Pending, output.PaymentRequests[0].Status);
        }

        [Fact]
        public async Task AddPaymentRequest_And_Process_IsSuccessful_PaymentIsProcessed()
        {
            var payload = new { Amount = 100, Date = new DateTime(2020, 8, 20) }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            postResponse.EnsureSuccessStatusCode();

            var response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);

            Assert.Single(output.PaymentRequests);

            //Process payment
            var processResponse = await _client.PostAsync($"/paymentrequest/{output.PaymentRequests[0].Id}/process", payload);
            processResponse.EnsureSuccessStatusCode();

            // Query payments again
            response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();
            responseString = await response.Content.ReadAsStringAsync();
            output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);

            //Balance is subtracted correctly
            Assert.Equal(400, output.AccountBalance);
            Assert.Single(output.PaymentRequests);
            Assert.Equal(Model.PaymentStatus.Processed, output.PaymentRequests[0].Status);
        }

        [Fact]
        public async Task AddPaymentRequest_AmountOverBalance_PaymentIsClosed()
        {
            var payload = new { Amount = 1000, Date = new DateTime(2020, 8, 20) }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            postResponse.EnsureSuccessStatusCode();


            var response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);

            Assert.Equal(1000, output.PaymentRequests[0].Amount);
            Assert.Equal(new DateTime(2020, 8, 20), output.PaymentRequests[0].Date);
            Assert.Equal(Model.PaymentStatus.Closed, output.PaymentRequests[0].Status);
        }

        [Fact]
        public async Task ProcessClosedRequest_ReturnsBadRequest()
        {
            var payload = new { Amount = 1000 /*over balance*/, Date = new DateTime(2020, 8, 20) }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            postResponse.EnsureSuccessStatusCode();

            var response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);

            Assert.Equal(Model.PaymentStatus.Closed, output.PaymentRequests[0].Status);


            var processResponse = await _client.PostAsync($"/paymentrequest/{output.PaymentRequests[0].Id}/process", null);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, processResponse.StatusCode);

            var error = await processResponse.Content.ReadAsStringAsync();
            Assert.Equal("Can only process a payment that is pending", error);
        }

        [Fact]
        public async Task ProcessCanceledRequest_ReturnsBadRequest()
        {
            var payload = new { Amount = 100, Date = new DateTime(2020, 8, 20) }.AsJson();

            var postResponse = await _client.PostAsync("/paymentrequest", payload);
            postResponse.EnsureSuccessStatusCode();

            var response = await _client.GetAsync("/paymentrequest");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            var output = JsonConvert.DeserializeObject<PaymentRequestsOutput>(responseString);
            Assert.Equal(Model.PaymentStatus.Pending, output.PaymentRequests[0].Status);

            var cancelPayload = new { }.AsJson();
            var cancelResponse = await _client.PostAsync($"/paymentrequest/{output.PaymentRequests[0].Id}/cancel", cancelPayload);
            cancelResponse.EnsureSuccessStatusCode();


            var processResponse = await _client.PostAsync($"/paymentrequest/{output.PaymentRequests[0].Id}/process", null);
            Assert.Equal(System.Net.HttpStatusCode.BadRequest, processResponse.StatusCode);
            
            var error = await processResponse.Content.ReadAsStringAsync();
            Assert.Equal("Can only process a payment that is pending", error);
        }
    }
}
