﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;

namespace Payment.Api.IntegrationTests
{
    public static class TestExtensions
    {
        public static StringContent AsJson(this object o)
        {
           return new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");
        }
    }
}
